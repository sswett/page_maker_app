class Page < ActiveRecord::Base

  # Given a page object, how do I get to a list of attached articles?
  # I get the keys? or records? via the following: TEST IT to see if this is all I need.
  # To TEST IT, seed the data.
  has_many :attached_articles, class_name: 'PageArticle', foreign_key: 'page_id', dependent: :destroy
  has_many :articles, through: :attached_articles

  # The above may get me to the PageArticle, but may not get me back to the Article itself -- for fetching the
  # content.  That's why I might need another relationship.

  default_scope -> { order(:title) }

  validates :title, presence: true, length: { maximum: 255 }

end
