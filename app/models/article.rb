class Article < ActiveRecord::Base

  has_many :associated_articles, class_name: 'PageArticle', foreign_key: 'article_id', dependent: :destroy
  has_many :pages, through: :associated_articles

  default_scope -> { order(:title) }

  mount_uploader :picture_file, PictureUploader
  mount_uploader :movie_file, MovieUploader

  validates :title, presence: true, length: { maximum: 255 }
  validates :main_article_header, presence: true, length: { maximum: 255 }
  validates :main_article_body, presence: true
  validates :main_article_footer, length: { maximum: 255 }
  validates :side_article_header, length: { maximum: 255 }
  validates :side_article_footer, length: { maximum: 255 }
  validates :picture_alt_text, length: { maximum: 255 }
  validates :picture_caption, length: { maximum: 255 }
  validates :movie_alt_text, length: { maximum: 255 }
  validates :movie_caption, length: { maximum: 255 }

  validate :picture_size   # See custom validator below
  validate :movie_size   # See custom validator below


  def get_main_article_body_as_tokens
    main_article_body.split(/(!picture)|(!movie)|(!break)/)
  end


  def get_side_article_body_as_tokens
    side_article_body.split(/(!picture)|(!movie)|(!break)/)
  end


  private


  # Validates the size of an uploaded picture.
  def picture_size
    if picture_file.size > 3.megabytes
      errors.add(:picture_file, 'should be less than 3MB')
    end
  end


  # Validates the size of an uploaded movie.
  def movie_size
    if movie_file.size > 10.megabytes
      errors.add(:movie_file, 'should be less than 10MB')
    end
  end


end
