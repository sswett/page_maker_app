module PagesHelper


  def get_main_article_elem_class(article)
    if article.side_article_body.empty?
      'col-md-12'
    else
      'col-md-7'
    end
  end


  def get_side_article_elem_class(article)
    if article.side_article_body.empty?
      'hidden'
    else
      'col-md-5'
    end
  end


end
