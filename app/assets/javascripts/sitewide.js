function readInputFileIntoSrcAttr(pInputFileSelector, pElemForSrcSelector) {
    var inputElem = $(pInputFileSelector)[0];

    if (inputElem.files && inputElem.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(pElemForSrcSelector).attr('src', e.target.result);
        }

        reader.readAsDataURL(inputElem.files[0]);
    }
}


function createUploadFileOnChangeHandler(pInputFileSelector, pPictureOrMovieStr, pMaxSizeMb,
                                         pExistingFileNameSpanSelector, pExistingFilePreviewElemSelector)
{
    $(pInputFileSelector).bind('change', function() {
        size_in_megabytes = this.files[0].size/1024/1024;

        if (size_in_megabytes > pMaxSizeMb) {
            alert('Maximum ' + pPictureOrMovieStr + ' file size is ' +
                pMaxSizeMb + 'MB. Please choose a smaller file.');
        }
        else {
            $(pExistingFileNameSpanSelector).hide();
            $(pExistingFilePreviewElemSelector).removeClass('zero_height');
            $("#" + pPictureOrMovieStr + "_container").removeClass('zero_height');
            readInputFileIntoSrcAttr(pInputFileSelector, pExistingFilePreviewElemSelector);
        }
    });

}