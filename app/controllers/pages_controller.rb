class PagesController < ApplicationController

  before_action :prep_form_related_actions,   only: [:create, :new, :edit]


  def index   # i.e. /pages with GET
    @pages = Page.paginate(page: params[:page], per_page: 10)
  end


  def show   # i.e. /pages/1 with GET
    @page = Page.find(params[:id])

    panel_1_page_article = @page.attached_articles.find_by(panel_number: 1)
    panel_2_page_article = @page.attached_articles.find_by(panel_number: 2)
    panel_3_page_article = @page.attached_articles.find_by(panel_number: 3)
    panel_4_page_article = @page.attached_articles.find_by(panel_number: 4)

    @panel_1_article = panel_1_page_article == nil ? nil : @page.articles.find_by(id: panel_1_page_article.article_id)
    @panel_2_article = panel_2_page_article == nil ? nil : @page.articles.find_by(id: panel_2_page_article.article_id)
    @panel_3_article = panel_3_page_article == nil ? nil : @page.articles.find_by(id: panel_3_page_article.article_id)
    @panel_4_article = panel_4_page_article == nil ? nil : @page.articles.find_by(id: panel_4_page_article.article_id)

    @panel_1_style = panel_1_page_article == nil ? '1' : panel_1_page_article.style_number
    @panel_2_style = panel_2_page_article == nil ? '1' : panel_2_page_article.style_number
    @panel_3_style = panel_3_page_article == nil ? '1' : panel_3_page_article.style_number
    @panel_4_style = panel_4_page_article == nil ? '1' : panel_4_page_article.style_number
  end


  def edit   # i.e. /pages/1/edit with GET
  end


  def create   # /pages with POST

    # The following is redundant with code block in update function, but I need to ask somebody about proper way
    # to do this.  I tried using a "before_action" call without any luck.
    if !panel_entries_valid?
      flash[:danger] = 'If an article or style is specified for a panel, both are required.'
      preserve_form_fields_on_error
      render :new and return
    end

    if @page.save
      save_panel_entries(@page)
      flash[:success] = 'New page has been saved.'
      # render 'edit'
      # redirect_to edit_page_url   # This solved problem with re-displaying newly added attached articles
      redirect_to edit_page_path(@page)
    else
      preserve_form_fields_on_error
      render :new   # try again
    end
  end


  def update   # /pages/1 with PATCH
    @page = Page.find(params[:id])

    if !panel_entries_valid?
      flash[:danger] = 'If an article or style is specified for a panel, both are required.'
      #redirect_to edit_page_url and return
      preserve_form_fields_on_error
      render :edit and return
    end

    if @page.update_attributes(permitted_params)
      save_panel_entries(@page)
      flash[:success] = 'Page saved'
    end

    # render 'edit'
    redirect_to edit_page_url   # This solved problem with re-displaying newly added attached articles
  end


  def destroy   # /pages/1 with DELETE
    Page.find(params[:id]).destroy
    flash[:success] = 'Page deleted'
    redirect_to pages_url
  end


  def new   # /pages/new with GET
  end


  private


  def permitted_params
    # not sure why content assist doesn't work here; perhaps cuz this is so general as not to know about the
    # Page object
    params.require(:page).permit(:title)

  end


  # The following shouldn't be necessary if the binding between the form and models was improved or more
  # standard.  Need to ask someone.  Perhaps if validation were done in the models, this wouldn't be necessary.
  # However, a custom validation isn't obvious due to model vs. GUI.

  def preserve_form_fields_on_error

    setup_common_instance_vars

    @page.title = params[:page][:title]

    @pa_article_id = { panel_1: params[:article_id][:panel_1],
                       panel_2: params[:article_id][:panel_2],
                       panel_3: params[:article_id][:panel_3],
                       panel_4: params[:article_id][:panel_4]
    }

    @pa_style_number = { panel_1: params[:style_number][:panel_1],
                         panel_2: params[:style_number][:panel_2],
                         panel_3: params[:style_number][:panel_3],
                         panel_4: params[:style_number][:panel_4]
    }

  end


  def setup_common_instance_vars
    @all_articles = Article.all

    # Perhaps in the future this could be something other than hard-wired
    @all_styles = [
        { id: 1, title: 'Style 1'},
        { id: 2, title: 'Style 2'},
        { id: 3, title: 'Style 3'},
    ]
  end


  def prep_form_related_actions
    # @page = action_name == 'new' ? Page.new : Page.find(params[:id])

    case action_name
      when 'new'
        @page = Page.new
      when 'create'
        @page = Page.new(permitted_params)
      else
        @page = Page.find(params[:id])
    end

    setup_common_instance_vars

    panel_1_article = @page.attached_articles.find_by(panel_number: 1)
    panel_2_article = @page.attached_articles.find_by(panel_number: 2)
    panel_3_article = @page.attached_articles.find_by(panel_number: 3)
    panel_4_article = @page.attached_articles.find_by(panel_number: 4)

    @pa_article_id = { panel_1: panel_1_article ? panel_1_article.article_id : nil,
                       panel_2: panel_2_article ? panel_2_article.article_id : nil,
                       panel_3: panel_3_article ? panel_3_article.article_id : nil,
                       panel_4: panel_4_article ? panel_4_article.article_id : nil
    }

    @pa_style_number = { panel_1: panel_1_article ? panel_1_article.style_number : nil,
                         panel_2: panel_2_article ? panel_2_article.style_number : nil,
                         panel_3: panel_3_article ? panel_3_article.style_number : nil,
                         panel_4: panel_4_article ? panel_4_article.style_number : nil

    }

  end


  def panel_entries_valid?

    4.times do |n|
      tmp_panel_number = n + 1
      tmp_article_id = params[:article_id]["panel_#{tmp_panel_number}".to_sym]
      tmp_style_number = params[:style_number]["panel_#{tmp_panel_number}".to_sym]

      article_present = tmp_article_id && !tmp_article_id.empty?
      style_present = tmp_style_number && !tmp_style_number.empty?

      if ( (article_present && !style_present) || (!article_present && style_present) )
        return false
      end

    end

    return true
  end


  # Not sure if this should be here or in a helper file
  def save_panel_entries(page_instance)
    # First delete all existing PageArticle records:
    # PageArticle.find_by_page_id(page_instance.id).destroy
    PageArticle.destroy_all(page_id: page_instance.id)

    # Then add PageArticle records, if any specified:
    4.times do |n|
      tmp_panel_number = n + 1
      tmp_article_id = params[:article_id]["panel_#{tmp_panel_number}".to_sym]

      if tmp_article_id && !tmp_article_id.empty?
        tmp_style_number = params[:style_number]["panel_#{tmp_panel_number}".to_sym]

        # PageArticle.create! ...
        page_instance.attached_articles.create!(page_id: page_instance.id,
                                                article_id: tmp_article_id,
                                                panel_number: tmp_panel_number,
                                                style_number: tmp_style_number)
      end

    end

  end


end
