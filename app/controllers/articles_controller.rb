class ArticlesController < ApplicationController


  def index   # i.e. /articles with GET
    @articles = Article.paginate(page: params[:page], per_page: 10)
  end


  def show   # i.e. /articles/1 with GET
    @article = Article.find(params[:id])
  end


  def edit   # i.e. /articles/1/edit with GET
    @article = Article.find(params[:id])
  end


  def create   # /articles with POST
    @article = Article.new(permitted_params)

    if @article.save
      flash[:info] = 'New article has been saved.'
      # redirect_to articles_url
      render 'edit'
    else
      render 'new'   # try again
      # render 'edit'
      # redirect_to edit_article_path(@article)
    end
  end


  def update   # /articles/1 with PATCH
    @article = Article.find(params[:id])

    if @article.update_attributes(permitted_params)
      flash[:success] = 'Article saved'
    end

    render 'edit'
  end


  def destroy   # /articles/1 with DELETE
    Article.find(params[:id]).destroy
    flash[:success] = 'Article deleted'
    redirect_to articles_url
  end


  def new   # /articles/new with GET
    @article = Article.new
  end


  private

  def permitted_params
    # not sure why content assist doesn't work here; perhaps cuz this is so general as not to know about the
    # Article object
    params.require(:article).permit(:title, :main_article_header, :main_article_body, :main_article_footer,
                                 :side_article_header, :side_article_body, :side_article_footer,
                                 :picture_file, :picture_alt_text, :picture_caption,
                                 :movie_file, :movie_alt_text, :movie_caption)
  end


end
