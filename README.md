## Introduction ##

This sample application was developed by Steve Swett. The point is to demonstrate my ability to develop a basic application using Ruby on Rails. It serves no practical end-user purpose. In fact, in order to limit my time investment, the UI/UX was not designed to be slick and convenient (no fancy inline editing here). The app is used to create articles and "pages" (a collection of articles).

The code is not intended to be used or collaboratively worked on or improved.  It is here for review purposes only.

The following technologies were used:

* Ruby on Rails (including SASS for CSS, REST)
* HTML 5
* jQuery
* Amazon Simple Storage Service (S3) - for storing uploaded pictures and movies
* Bootstrap
* Bitbucket (for remote VCS repository)
* Heroku and Heroku Toolbelt (for production deployment)
* RubyMine IDE on Windows (for editing, debugging, testing, etc.)

## What does the App do? ##

It simply allows you to work with two types of resources: Articles and Pages. Each Article resource actually has two articles: a "main" and a "side." An optional picture and movie can be uploaded to an article. The picture and movie can be placed anywhere within an article body.

A page is simply a collection of up to 4 articles. There are 4 panels on a page. You can choose the article and article style (basic appearance) for each panel.

Articles should be created first and previewed with the Show option.  Pages should then be created and previewed with Show.  There is nothing more you can do with pages.

## Running the App ##

To run the app live in production go [here](https://aqueous-cove-7409.herokuapp.com/).

## Screen Shots ##

The app consists of 9 rendered pages, but only two are shown here.

### Example of a 4-Panel Page ###

Here is a picture of a 4-panel page previewed with Show:

![show_page.png](https://bitbucket.org/repo/genK6o/images/2647878554-show_page.png)

### Example of Editing an Article ###

Here is a picture of the page used to edit an article.  (A simple form-based approach is used to edit both Articles and Pages.)

![editing_article.png](https://bitbucket.org/repo/genK6o/images/1826645373-editing_article.png)

## Database ##

The app uses Sqlite3 for development and tests and Postgres for production.  Instead of working with the database directly, the Rails way was followed.  Rails ActiveRecord data models were created first, and the database was populated via db migrations and seeding.  The database consists of 3 tables:

* articles
* pages
* page_articles

### Schemas ###

Following are the schemas for the 3 tables:

![db_schema.png](https://bitbucket.org/repo/genK6o/images/2539771188-db_schema.png)

### Database Seeding ###

To automatically preload (and reset) the development and production databases, the following **seeds.rb** Ruby script is used:


```
#!Ruby

def create_article(title,
                   main_article_hdr, main_article_bod, main_article_ftr,
                   side_article_hdr, side_article_bod, side_article_ftr)

  Article.create!(title: title,
                  main_article_header: main_article_hdr,
                  main_article_body: main_article_bod,
                  main_article_footer: main_article_ftr,
                  side_article_header: side_article_hdr,
                  side_article_body: side_article_bod,
                  side_article_footer: side_article_ftr)

end


def add_articles_to_pages(articles_per_page, pages)
  pages.each do |page|
    panel_num = 0
    articles_per_page.each do |article|
      panel_num += 1

      page.attached_articles.create!(article_id: article.id,
                                     panel_number: panel_num,
                                     style_number: 1 + rand(3))
    end
  end
end


# Do article seeding:


# Create 4 demo articles:

article_1 = create_article('Article 1 - Onaway 4th of July Tanner Pics',
                           'Pictures of Tanner July 2014',
                           'Tanner and I made a trip to Onaway for the 4th of July weekend.  Along the way, I stopped and took a few pictures of Tanner -- especially when there was something unusual near which to pose.  The picture below comes from near the "blinker light" intersection in town.
!picture
The Statue of Liberty was fabricated by Moran Iron Works for some past 4th of July parade.',
                           'by S Swett 2014',
                           'Tanner was a Good Sport',
                           'Tanner had his picture taken with a monster truck, a bear, partridge, deer, a bobcat, an opossum, a muskrat, farm equipment and other northern/redneck oddities.  He was a good sport through all this.',
                           'posted Dec 2014')

article_2 = create_article('Article 2 - Onaway Steers the World',
                           'Onaway Steers the World',
                           'In recent years, local Moran Iron Works has fabricated an entry for the 4th of July parade.  The 2014 entry:
!picture
The huge steering wheel had the look of wood grain.  The giant wheel could be lowered/raised en route to avoid power lines.',
                           'July 4 2014',
                           'Moran Iron Works',
                           'Moran Iron Works is big business in Onaway.  They have received multi-million dollar contracts, and fabricate for companies all over the world.  They often truck their finished products to Rogers City where they are transferred to boat for delivery.  They provide employment for dozens of local workers.
!break
Tom Moran is the owner.',
                           'written by Jimbo Bobby')

article_3 = create_article('Article 3 - Picking Guitar with Drill',
                           'Rednecks, Tools and Music',
                           'This guy must have been looking for a new way to entertain himself.
!movie
Hey, if you have an idea, go for it!',
                           '(grabbed from the internet)',
                           'Rednecks',
                           'If you have any interesting redneck stories, please let us know.',
                           '')

article_4 = create_article('Article 4 - Info about Articles - Text Only',
                           'About article panels',
                           'The article panels can optionally contain a picture, a movie, text and a side article.  Article footers are also optional.',
                           '',
                           '',
                           '',
                           '')

demo_articles = [ article_1, article_2, article_3, article_4 ]


# Create 30 sample articles:

30.times do |n|
  article_no = (n + 1).to_s.rjust(2, '0')
  title = "Sample Article #{article_no} Title"
  main_article_hdr = 'Main Article Header'
  main_article_bod = Faker::Lorem.sentence(12)
  main_article_ftr = 'Main Article Footer'
  side_article_hdr = 'Side Article Header'
  side_article_bod = Faker::Lorem.sentence(6)
  side_article_ftr = 'Side Article Footer'

  # Seems like you should be able to get content assist for filling out the attributes below.

  create_article(title,
                 main_article_hdr, main_article_bod, main_article_ftr,
                 side_article_hdr, side_article_bod, side_article_ftr)

end


# Do page seeding:

# Create 1 demo page:

demo_pages = [ Page.create!(title: 'A Page Demonstrating 4 Articles in 4 Panels') ]

# Create 30 sample pages:

30.times do |n|
  page_no = (n + 1).to_s.rjust(2, '0')
  title = "Sample Page #{page_no} Title"
  Page.create!(title: title)
end


# Do page article seeding:

# Create demo page articles:

add_articles_to_pages(demo_articles, demo_pages)


# Create sample page articles:

where_clause = "title LIKE '#{'Sample'}%'"
articles = Article.where( where_clause ).order(:title).take(4);   # grab first 4 samples
pages = Page.where( where_clause ).order(:title).take(5);   # grab first 5 samples


add_articles_to_pages(articles, pages)

```



## Select Source Files ##

Obviously, you can view all the current source code in the project by navigating to the Source area of this repository.  You can also see the project/directory structure that way.  (The structure follows the Rails way.)  For the purposes of providing a general overview, a few select source files are called out here.

### Application-Wide Layout View ###

The **application.html.erb** file is shown below.  This is the only place where full page HTML markup is defined (with html, head and body tags).  All other views are partial files that are included or rendered in as needed.


```
#!ERB

<!DOCTYPE html>
<html>
<head>
  <title><%= full_title(yield(:title)) %></title>
  <%= stylesheet_link_tag 'application', media: 'all',
                          'data-turbolinks-track' => true %>
  <%= javascript_include_tag 'application', 'data-turbolinks-track' => true %>
  <%= csrf_meta_tags %>
  <%= render 'layouts/shim' %>
</head>
<body>
<%= render 'layouts/header' %>
<div class="container">

  <% flash.each do |message_type, message| %>
      <div class="alert alert-<%= message_type %>"><%= message %></div>
  <% end %>

  <%= yield %>
  <%= render 'layouts/footer' %>
  <%= debug(params) if Rails.env.development? %>
</div>
</body>
</html>

```

### View for Page Show Action (4 panels of articles) ###

Here is the **show.html.erb** file used to render the 4-article, 4-panel page shown in the first screen shot above:


```
#!ERB
<% provide(:title, @page.title) %>

<div class="row">
  <h2>Page Details - <%= @page.title %> <small><%= link_to '[edit]', edit_page_path(@page) %></small></h2>
</div>

<div class="row first_panel_row">
  <%= render 'shared/one_panel', p_article: @panel_1_article, p_style_number: @panel_1_style, p_panel_number: 1 %>
  <%= render 'shared/one_panel', p_article: @panel_2_article, p_style_number: @panel_2_style, p_panel_number: 2 %>
</div>

<div class="row second_panel_row">
  <%= render 'shared/one_panel', p_article: @panel_3_article, p_style_number: @panel_3_style, p_panel_number: 3 %>
  <%= render 'shared/one_panel', p_article: @panel_4_article, p_style_number: @panel_4_style, p_panel_number: 4 %>
</div>


<%- # The rest of the page is only output in a development environment -%>

<% if Rails.env.development? %>
<div class="page_article_data row">

  <div>
    Attached articles:
  </div>

  <ul>
    <% @page.attached_articles.each do |attached_article| %>
        <li>
          Page id: <%= attached_article.page_id %>,
          Article id: <%= attached_article.article_id %>,
          Panel number: <%= attached_article.panel_number %>,
          Style number: <%= attached_article.style_number %>
        </li>
    <% end %>
  </ul>

  <div>
    Attached article content:
  </div>

  <ul>
    <% @page.articles.each do |article| %>
        <li>
          Id: <%= article.id %>,
          Title: <%= article.title %>,
          Main article hdr: <%= article.main_article_header %>
        </li>
    <% end %>
  </ul>

</div>
<% end %>
```

### Model for Articles ###

The **article.rb** file is an example of a model -- in this case the ActiveRecord model for the data in the articles table.  Custom validator methods are present for validating the size of pictures and movies.


```
#!Ruby

class Article < ActiveRecord::Base

  has_many :associated_articles, class_name: 'PageArticle', foreign_key: 'article_id', dependent: :destroy
  has_many :pages, through: :associated_articles

  default_scope -> { order(:title) }

  mount_uploader :picture_file, PictureUploader
  mount_uploader :movie_file, MovieUploader

  validates :title, presence: true, length: { maximum: 255 }
  validates :main_article_header, presence: true, length: { maximum: 255 }
  validates :main_article_body, presence: true
  validates :main_article_footer, length: { maximum: 255 }
  validates :side_article_header, length: { maximum: 255 }
  validates :side_article_footer, length: { maximum: 255 }
  validates :picture_alt_text, length: { maximum: 255 }
  validates :picture_caption, length: { maximum: 255 }
  validates :movie_alt_text, length: { maximum: 255 }
  validates :movie_caption, length: { maximum: 255 }

  validate :picture_size   # See custom validator below
  validate :movie_size   # See custom validator below


  def get_main_article_body_as_tokens
    main_article_body.split(/(!picture)|(!movie)|(!break)/)
  end


  def get_side_article_body_as_tokens
    side_article_body.split(/(!picture)|(!movie)|(!break)/)
  end


  private


  # Validates the size of an uploaded picture.
  def picture_size
    if picture_file.size > 3.megabytes
      errors.add(:picture_file, 'should be less than 3MB')
    end
  end


  # Validates the size of an uploaded movie.
  def movie_size
    if movie_file.size > 10.megabytes
      errors.add(:movie_file, 'should be less than 10MB')
    end
  end


end

```

### Controller for Articles ###

The **articles_controller.rb** file is an example of a controller -- in this case the one for interfacing with the Articles model and the various views.  This controller shows the classic actions, URIs and http methods associated with RESTful resource architecture.


```
#!Ruby

class ArticlesController < ApplicationController


  def index   # i.e. /articles with GET
    @articles = Article.paginate(page: params[:page], per_page: 10)
  end


  def show   # i.e. /articles/1 with GET
    @article = Article.find(params[:id])
  end


  def edit   # i.e. /articles/1/edit with GET
    @article = Article.find(params[:id])
  end


  def create   # /articles with POST
    @article = Article.new(permitted_params)

    if @article.save
      flash[:info] = 'New article has been saved.'
      # redirect_to articles_url
      render 'edit'
    else
      render 'new'   # try again
      # render 'edit'
      # redirect_to edit_article_path(@article)
    end
  end


  def update   # /articles/1 with PATCH
    @article = Article.find(params[:id])

    if @article.update_attributes(permitted_params)
      flash[:success] = 'Article saved'
    end

    render 'edit'
  end


  def destroy   # /articles/1 with DELETE
    Article.find(params[:id]).destroy
    flash[:success] = 'Article deleted'
    redirect_to articles_url
  end


  def new   # /articles/new with GET
    @article = Article.new
  end


  private

  def permitted_params
    # not sure why content assist doesn't work here; perhaps cuz this is so general as not to know about the
    # Article object
    params.require(:article).permit(:title, :main_article_header, :main_article_body, :main_article_footer,
                                 :side_article_header, :side_article_body, :side_article_footer,
                                 :picture_file, :picture_alt_text, :picture_caption,
                                 :movie_file, :movie_alt_text, :movie_caption)
  end


end

```

### Articles Integration Test ###

The following file, **articles_interface_test.rb**, contains integration tests associated with workflow related to creating and editing articles.


```
#!Ruby

require 'test_helper'

class ArticlesInterfaceTest < ActionDispatch::IntegrationTest


  def setup
    @article = articles(:good_one)
  end


  test 'articles link on home page' do
    get root_path
    # assert_select 'a[href=/articles]'   # doesn't work; syntax/escape problem
    assert_select 'a[href=?]', '/articles'
  end


  test 'create invalid simple new article' do
    # Go to articles page
    get articles_path
    assert_template 'articles/index'
    assert_select 'a[href=?]', '/articles/new'

    # Go to create new article page
    get new_article_path
    assert_template 'articles/new'

    # Attempt to create an article with no title or other field values
    assert_no_difference 'Article.count' do
      post articles_path, article: { title: '' }
    end

    assert_select 'div#error_explanation'

  end


  test 'create invalid new article picture size too big' do
    # Go to articles page
    get articles_path
    assert_template 'articles/index'
    assert_select 'a[href=?]', '/articles/new'

    # Go to create new article page
    get new_article_path
    assert_template 'articles/new'

    picture = fixture_file_upload('test/fixtures/size_too_big.jpg', 'image/jpeg')

    # Attempt to create an article with an uploaded picture that is too big:
    assert_no_difference 'Article.count' do
      post articles_path, article: { title: 'my title', main_article_header: 'main header',
                                     main_article_body: 'main body', picture_file: picture }
    end

    assert_select 'div#error_explanation'

  end


  test 'create invalid new article movie size too big' do
    movie = fixture_file_upload('test/fixtures/size_too_big.mp4', 'video/mp4')

    # Attempt to create an article with an uploaded movie that is too big:
    assert_no_difference 'Article.count' do
      post articles_path, article: { title: 'my title', main_article_header: 'main header',
                                     main_article_body: 'main body', movie_file: movie }
    end

    assert_select 'div#error_explanation'

  end


  test 'create valid simple new article' do
    # Go to create new article page
    get new_article_path
    assert_template 'articles/new'

    assert_difference 'Article.count', 1 do
      post articles_path, article: { title: 'my title', main_article_header: 'main header',
                                     main_article_body: 'main body' }
    end

    assert_select 'div[class=?]', 'alert alert-info'
    assert_match 'New article has been saved.', response.body
  end


  test 'create valid new article picture size ok' do
    # Go to create new article page
    get new_article_path
    assert_template 'articles/new'

    picture = fixture_file_upload('test/fixtures/size_ok.jpg', 'image/jpeg')

    assert_difference 'Article.count', 1 do
      post articles_path, article: { title: 'my title', main_article_header: 'main header',
                                     main_article_body: 'main body', picture_file: picture }
    end

    assert_select 'div[class=?]', 'alert alert-info'
    assert_match 'New article has been saved.', response.body
  end


  test 'create valid new article movie size ok' do
    movie = fixture_file_upload('test/fixtures/size_ok.mp4', 'video/mp4')

    assert_difference 'Article.count', 1 do
      post articles_path, article: { title: 'my title', main_article_header: 'main header',
                                     main_article_body: 'main body', movie_file: movie }
    end

    assert_select 'div[class=?]', 'alert alert-info'
    assert_match 'New article has been saved.', response.body
  end


  test 'edit existing article' do
    get edit_article_path(@article)
    assert_template 'articles/edit'
    footer = 'my footer'
    patch article_path(@article), article: { main_article_footer:  footer }
    #assert_select 'div[class=?]', 'alert alert-info'
    #assert_match 'Article saved', response.body
    @article.reload
    assert_equal @article.main_article_footer, footer
  end


  test 'delete article' do
    assert_difference 'Article.count', -1 do
      delete article_path(@article)
    end
  end


end

```

## Conclusion ##

This overview has provided the following:

* general info on what the app is and does
* a link to the live, production app
* select screen shots
* database info
* select source files, including models, views, controllers and tests

Feel free to explore the source repository to see:

* gems used
* route definitions
* CSS and Javascript in the asset pipeline
* picture/movie uploaders
* use of shared, partial views
* more tests and fixtures