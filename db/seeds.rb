# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


def create_article(title,
                   main_article_hdr, main_article_bod, main_article_ftr,
                   side_article_hdr, side_article_bod, side_article_ftr)

  Article.create!(title: title,
                  main_article_header: main_article_hdr,
                  main_article_body: main_article_bod,
                  main_article_footer: main_article_ftr,
                  side_article_header: side_article_hdr,
                  side_article_body: side_article_bod,
                  side_article_footer: side_article_ftr)

end


def add_articles_to_pages(articles_per_page, pages)
  pages.each do |page|
    panel_num = 0
    articles_per_page.each do |article|
      panel_num += 1

      page.attached_articles.create!(article_id: article.id,
                                     panel_number: panel_num,
                                     style_number: 1 + rand(3))
    end
  end
end


# Do article seeding:


# Create 4 demo articles:

article_1 = create_article('Article 1 - Onaway 4th of July Tanner Pics',
                           'Pictures of Tanner July 2014',
                           'Tanner and I made a trip to Onaway for the 4th of July weekend.  Along the way, I stopped and took a few pictures of Tanner -- especially when there was something unusual near which to pose.  The picture below comes from near the "blinker light" intersection in town.
!picture
The Statue of Liberty was fabricated by Moran Iron Works for some past 4th of July parade.',
                           'by S Swett 2014',
                           'Tanner was a Good Sport',
                           'Tanner had his picture taken with a monster truck, a bear, partridge, deer, a bobcat, an opossum, a muskrat, farm equipment and other northern/redneck oddities.  He was a good sport through all this.',
                           'posted Dec 2014')

article_2 = create_article('Article 2 - Onaway Steers the World',
                           'Onaway Steers the World',
                           'In recent years, local Moran Iron Works has fabricated an entry for the 4th of July parade.  The 2014 entry:
!picture
The huge steering wheel had the look of wood grain.  The giant wheel could be lowered/raised en route to avoid power lines.',
                           'July 4 2014',
                           'Moran Iron Works',
                           'Moran Iron Works is big business in Onaway.  They have received multi-million dollar contracts, and fabricate for companies all over the world.  They often truck their finished products to Rogers City where they are transferred to boat for delivery.  They provide employment for dozens of local workers.
!break
Tom Moran is the owner.',
                           'written by Jimbo Bobby')

article_3 = create_article('Article 3 - Picking Guitar with Drill',
                           'Rednecks, Tools and Music',
                           'This guy must have been looking for a new way to entertain himself.
!movie
Hey, if you have an idea, go for it!',
                           '(grabbed from the internet)',
                           'Rednecks',
                           'If you have any interesting redneck stories, please let us know.',
                           '')

article_4 = create_article('Article 4 - Info about Articles - Text Only',
                           'About article panels',
                           'The article panels can optionally contain a picture, a movie, text and a side article.  Article footers are also optional.',
                           '',
                           '',
                           '',
                           '')

demo_articles = [ article_1, article_2, article_3, article_4 ]


# Create 30 sample articles:

30.times do |n|
  article_no = (n + 1).to_s.rjust(2, '0')
  title = "Sample Article #{article_no} Title"
  main_article_hdr = 'Main Article Header'
  main_article_bod = Faker::Lorem.sentence(12)
  main_article_ftr = 'Main Article Footer'
  side_article_hdr = 'Side Article Header'
  side_article_bod = Faker::Lorem.sentence(6)
  side_article_ftr = 'Side Article Footer'

  # Seems like you should be able to get content assist for filling out the attributes below.

  create_article(title,
                 main_article_hdr, main_article_bod, main_article_ftr,
                 side_article_hdr, side_article_bod, side_article_ftr)

end


# Do page seeding:

# Create 1 demo page:

demo_pages = [ Page.create!(title: 'A Page Demonstrating 4 Articles in 4 Panels') ]

# Create 30 sample pages:

30.times do |n|
  page_no = (n + 1).to_s.rjust(2, '0')
  title = "Sample Page #{page_no} Title"
  Page.create!(title: title)
end


# Do page article seeding:

# Create demo page articles:

add_articles_to_pages(demo_articles, demo_pages)


# Create sample page articles:

where_clause = "title LIKE '#{'Sample'}%'"
articles = Article.where( where_clause ).order(:title).take(4);   # grab first 4 samples
pages = Page.where( where_clause ).order(:title).take(5);   # grab first 5 samples


add_articles_to_pages(articles, pages)


