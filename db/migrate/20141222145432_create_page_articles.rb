class CreatePageArticles < ActiveRecord::Migration
  def change

    create_table :page_articles do |t|
      t.integer :page_id
      t.integer :article_id
      t.integer :panel_number
      t.integer :style_number

      t.timestamps null: false
    end

    add_index :page_articles, :page_id
    add_index :page_articles, :article_id
    add_index :page_articles, [:page_id, :article_id], unique: true

  end
end
