class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :main_header
      t.text :main_section
      t.string :main_footer
      t.string :aside_header
      t.text :aside_section
      t.string :aside_footer
      t.string :picture_file
      t.string :picture_alt_text
      t.string :picture_caption
      t.string :movie_file
      t.string :movie_alt_text
      t.string :movie_caption

      t.timestamps null: false
    end
  end
end
