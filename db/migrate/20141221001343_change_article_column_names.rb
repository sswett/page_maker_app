class ChangeArticleColumnNames < ActiveRecord::Migration
  def change
    change_table :articles do |t|
      t.rename :main_header, :main_article_header
      t.rename :main_section, :main_article_body
      t.rename :main_footer, :main_article_footer
      t.rename :aside_header, :side_article_header
      t.rename :aside_section, :side_article_body
      t.rename :aside_footer, :side_article_footer
    end
  end
end
