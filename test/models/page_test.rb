require 'test_helper'

class PageTest < ActiveSupport::TestCase


  def setup
    @page = Page.new(title: 'Page Title')
  end


  test 'page should be valid' do
    assert @page.valid?
  end


  test 'title should be present' do
    @page.title = ''
    assert_not @page.valid?
  end


  test 'title should not be too long' do
    @page.title = 'x' * 256
    assert_not @page.valid?
  end

end
