require 'test_helper'

class ArticleTest < ActiveSupport::TestCase


  def setup
    # @article = articles(:good_one)   # also works, using fixtures

    @article = Article.new(title: 'Article Title',
                           main_article_header: 'Main Header', main_article_body: 'Main Body',
                           main_article_footer: 'Main Footer', side_article_header: 'Side Header',
                           side_article_body: 'Side Body', side_article_footer: '"Side Footer')

  end


  test 'article should be valid' do
    assert @article.valid?
  end


  test 'main article header should be present' do
    @article.main_article_header = ''
    assert_not @article.valid?
  end


  test 'main article header should not be too long' do
    @article.main_article_header = 'x' * 256
    assert_not @article.valid?
  end


  test 'main article body should be present' do
    @article.main_article_body = ''
    assert_not @article.valid?
  end


  test 'main article footer should not be too long' do
    @article.main_article_footer = 'x' * 256
    assert_not @article.valid?
  end


  test 'side article header should not be too long' do
    @article.side_article_header = 'x' * 256
    assert_not @article.valid?
  end


  test 'side article footer should not be too long' do
    @article.side_article_footer = 'x' * 256
    assert_not @article.valid?
  end


  test 'picture alt text should not be too long' do
    @article.picture_alt_text = 'x' * 256
    assert_not @article.valid?
  end


  test 'picture caption should not be too long' do
    @article.picture_caption = 'x' * 256
    assert_not @article.valid?
  end


  test 'movie alt text should not be too long' do
    @article.movie_alt_text = 'x' * 256
    assert_not @article.valid?
  end


  test 'movie caption should not be too long' do
    @article.movie_caption = 'x' * 256
    assert_not @article.valid?
  end


# Picture and movie sizes likely need to be tested in integration tests instead of here.

end
