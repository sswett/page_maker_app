require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase


  def setup
    @article = articles(:good_one)
  end


  test 'should get index' do
    get :index
    assert_response :success
    assert_select 'title', 'All Articles | Page Maker Sample App'
    assert_select 'h1', 'All Articles'
  end


  test 'should get show' do
    get :show, id: @article
    assert_response :success
    assert_match '<h2>Article Details', response.body
  end


  test 'should get edit' do
    get :edit, id: @article
    assert_response :success
    assert_match '<h2>Edit Article', response.body
  end


  test 'should get new' do
    get :new
    assert_response :success
    assert_match '<h2>Create New Article', response.body
  end


  test 'should patch update' do
    patch :show, id: @article
    assert_response :success
  end

# Likely need to run create/destroy tests within integration tests

end
