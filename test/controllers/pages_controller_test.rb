require 'test_helper'

class PagesControllerTest < ActionController::TestCase


  def setup
    @page = pages(:good_one)
  end


  test 'should get index' do
    get :index
    assert_response :success
    assert_select 'title', 'All Pages | Page Maker Sample App'
    assert_select 'h1', 'All Pages'
  end


  test 'should get show' do
    get :show, id: @page
    assert_response :success
    assert_match '<h2>Page Details', response.body
  end


  test 'should get edit' do
    get :edit, id: @page
    assert_response :success
    assert_match '<h2>Edit Page', response.body
  end


  test 'should get new' do
    get :new
    assert_response :success
    assert_match '<h2>Create New Page', response.body
  end


  test 'should patch update' do
    patch :show, id: @page
    assert_response :success
  end

# Likely need to run create/destroy tests within integration tests

end
