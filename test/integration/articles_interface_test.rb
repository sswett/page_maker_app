require 'test_helper'

class ArticlesInterfaceTest < ActionDispatch::IntegrationTest


  def setup
    @article = articles(:good_one)
  end


  test 'articles link on home page' do
    get root_path
    # assert_select 'a[href=/articles]'   # doesn't work; syntax/escape problem
    assert_select 'a[href=?]', '/articles'
  end


  test 'create invalid simple new article' do
    # Go to articles page
    get articles_path
    assert_template 'articles/index'
    assert_select 'a[href=?]', '/articles/new'

    # Go to create new article page
    get new_article_path
    assert_template 'articles/new'

    # Attempt to create an article with no title or other field values
    assert_no_difference 'Article.count' do
      post articles_path, article: { title: '' }
    end

    assert_select 'div#error_explanation'

  end


  test 'create invalid new article picture size too big' do
    # Go to articles page
    get articles_path
    assert_template 'articles/index'
    assert_select 'a[href=?]', '/articles/new'

    # Go to create new article page
    get new_article_path
    assert_template 'articles/new'

    picture = fixture_file_upload('test/fixtures/size_too_big.jpg', 'image/jpeg')

    # Attempt to create an article with an uploaded picture that is too big:
    assert_no_difference 'Article.count' do
      post articles_path, article: { title: 'my title', main_article_header: 'main header',
                                     main_article_body: 'main body', picture_file: picture }
    end

    assert_select 'div#error_explanation'

  end


  test 'create invalid new article movie size too big' do
    movie = fixture_file_upload('test/fixtures/size_too_big.mp4', 'video/mp4')

    # Attempt to create an article with an uploaded movie that is too big:
    assert_no_difference 'Article.count' do
      post articles_path, article: { title: 'my title', main_article_header: 'main header',
                                     main_article_body: 'main body', movie_file: movie }
    end

    assert_select 'div#error_explanation'

  end


  test 'create valid simple new article' do
    # Go to create new article page
    get new_article_path
    assert_template 'articles/new'

    assert_difference 'Article.count', 1 do
      post articles_path, article: { title: 'my title', main_article_header: 'main header',
                                     main_article_body: 'main body' }
    end

    assert_select 'div[class=?]', 'alert alert-info'
    assert_match 'New article has been saved.', response.body
  end


  test 'create valid new article picture size ok' do
    # Go to create new article page
    get new_article_path
    assert_template 'articles/new'

    picture = fixture_file_upload('test/fixtures/size_ok.jpg', 'image/jpeg')

    assert_difference 'Article.count', 1 do
      post articles_path, article: { title: 'my title', main_article_header: 'main header',
                                     main_article_body: 'main body', picture_file: picture }
    end

    assert_select 'div[class=?]', 'alert alert-info'
    assert_match 'New article has been saved.', response.body
  end


  test 'create valid new article movie size ok' do
    movie = fixture_file_upload('test/fixtures/size_ok.mp4', 'video/mp4')

    assert_difference 'Article.count', 1 do
      post articles_path, article: { title: 'my title', main_article_header: 'main header',
                                     main_article_body: 'main body', movie_file: movie }
    end

    assert_select 'div[class=?]', 'alert alert-info'
    assert_match 'New article has been saved.', response.body
  end


  test 'edit existing article' do
    get edit_article_path(@article)
    assert_template 'articles/edit'
    footer = 'my footer'
    patch article_path(@article), article: { main_article_footer:  footer }
    #assert_select 'div[class=?]', 'alert alert-info'
    #assert_match 'Article saved', response.body
    @article.reload
    assert_equal @article.main_article_footer, footer
  end


  test 'delete article' do
    assert_difference 'Article.count', -1 do
      delete article_path(@article)
    end
  end


end
